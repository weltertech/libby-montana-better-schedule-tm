// ==UserScript==
// @name         Libby Montana Volleyball: My Team Schedule
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  try to take over the world!
// @author       Brian Welter
// @match        http://www.libby-montana.com/vballsched.aspx
// @grant        none
// @require http://code.jquery.com/jquery-latest.js
// ==/UserScript==
(function() {
    'use strict';

   var $ = window.jQuery

   function addStyle () {
       var style = document.createElement('style');
       style.type = 'text/css';
       style.innerHTML = `
          .betterSchedule {
             background: white;
             padding: 15px;
          }
          .betterSchedule label {
             font-size: 1.2em;
             display: block;
             padding-bottom: 5px;
          }
          .betterSchedule li {
             width: 450px;
          }
          select {
             font-size: 1.2em;
             padding: 0.4em;
             background: white;
             border: 1px solid #50B557;
          }
          .bsItem {
             font-size: 1.2em;
             padding: 0.4em;
             background: white;
          }
          .bsItem.isnext {
             background: #50B557;
          }
          .bsItem:hover {
             font-size: 1.2em;
             padding: 0.4em;
             background: #eee;
          }
          .bsList {
             list-style-type: none;
             padding-left: 0;
          }
          .listHeader span {
             display: inline-block;
          }
          .bsItem span {
             display: inline-block;
          }
          .date {
             width: 200px;
          }
          .time {
             width: 150px;
          }
          .court {
             width: 50px;
          }
       `;
       document.getElementsByTagName('head')[0].appendChild(style);
   }

    function formatRecord(r) {
       return `<span class="date">${r.date}</span>
               <span class="time">${r.time}</span>
               <span class="court">${r.court}</span>`;
    }

    function getSchedule (team) {
         var schedule = []
         $("#GameSchedule tr[id^=ctl00_MainContent_lvSchedule_ctrl]").each(function () {
            var date = $(this).find("th:nth-child(2)").html().trim()
            var time = undefined
            var idx = 0
            $(this).next().find('.schedrow td').each(function () {
                if (time === undefined) {
                    time = $(this).html()
                } else {
                    if ($(this).html().indexOf(team) > -1) {
                        schedule.push({ date: date, court: idx, time: time.trim() })
                    }
                }
                if (idx == 3) {
                    idx = 0;
                } else {
                    idx++;
                }
            })
          });
       var listHtml = '<ul class="bsList">'
       var d = new Date();
       var n = d.getMilliseconds();

       var foundNext = false;

       listHtml += '<li class="listHeader">' + formatRecord({ date: 'Date', time: 'Time', court: 'Court' }) + '</li>';
       schedule.forEach(function (s) {
             var thisTime = Date.parse(s.date);
             var classes = 'bsItem';
             if (thisTime > n) {
                 foundNext = true;
                 classes += ' isnext';
             }
             listHtml += '<li class="' + classes + '">' + formatRecord(s) + '</li>';
       })

       $('#selectedTeamSchedule').html(listHtml);
    }

    function init () {
        addStyle ();
        var teams = []
        $("#standings .schedrow td:nth-child(1)").each(function () {
            var dotIdx = $(this).html().indexOf('. ');
            if ( dotIdx > -1) {
               teams.push($(this).html().substring(dotIdx + 2, $(this).html().length))
            }
        })

        let selHtml = `<div class="betterSchedule">
            <label for="teamSelect">Select Team</label>
            <select id="teamSelect" name="teamSelect">
                <option value="">SELECT TEAM</option>`;

        teams.forEach(function (t) {
            selHtml += '<option value="' + t + '">' + t + '</option>';
        })
        selHtml += '</select><div id="selectedTeamSchedule"></div></div>'

        $('form').prepend(selHtml);
    }

    $(document).ready(function () {
        init();
        $('#teamSelect').on('change', function() {
            if (this.value !== '') {
               localStorage.setItem('savedSelectedTeam', this.value);
               getSchedule(this.value);
            } else {
               getSchedule('-- i a not a team --');
            }
        });

        var savedTeamName = localStorage.getItem('savedSelectedTeam');

        if (savedTeamName) {
            $('#teamSelect').val(savedTeamName);
            getSchedule(savedTeamName);
        }

    });
})();
